// ------------------------------------------------------
// CHALLENGE B-1
// ------------------------------------------------------

// Given the quote data that is available in the `quotes` variable (defined below),
// Write a small application that will display each quote in a list in the DOM.
// Hint: look up the DOM APIs createElement() and appendChild()

var quotes = [
    {q: "Every man is born as many men and dies as a single one.", author: "Martin Heidegger"},
    {q: "Language is the house of the truth of Being.", author: "Martin Heidegger"},
    {q: "Man acts as though he were the shaper and master of language, while in fact language remains the master of man."},
    {q: "The most thought-provoking thing in our thought-provoking time is that we are still not thinking. ", author: "Martin Heidegger"},
    {q: "The possible ranks higher than the actual. ", author: "Martin Heidegger"},
    {q: "Unless you change how you are, you will always have what you've got. ", author: "Jim Rohn"},
    {q: "A stumble may prevent a fall. ", author: "English Proverb"},
    {q: "There's no limit to what a man can achieve, if he doesn't care who gets the credit. ", author: "Laing Burns, Jr."},
    {q: "Don't waste yourself in rejection, nor bark against the bad, but chant the beauty of the good.", author: "Ralph Waldo Emerson"},
    {q: "The most practical, beautiful, workable philosophy in the world won't work - if you won't. ", author: "Zig Ziglar"},
    {q: "I believe the greater the handicap, the greater the triumph. ", author: "John H. Johnson"},
    {q: "Life shrinks or expands in proportion to one's courage. ", author: "Anais Nin"},
    {q: "We don’t see things as they are we see them as we are. ", author: "Anais Nin"},
    {q: "You can't build a reputation on what you are going to do. ", author: "Henry Ford"},
    {q: "The greatest form of maturity is at harvest time. That is when we must learn how to reap without complaint if the amounts are small and how to to reap without apology if the amounts are big. ", author: "Jim Rohn"},
    {q: "People seem not to see that their opinion of the world is also a confession of character.  ", author: "Ralph Waldo Emerson"},
    {q: "The most successful people are those who are good at plan B. ", author: "James Yorke"},
    {q: "Opportunity is missed by most because it is dressed in overalls and looks like work. ", author: "Thomas Alva Edison"},
    {q: "The universe is full of magical things, patiently waiting for our wits to grow sharper. ", author: "Eden Phillpotts"},
    {q: "Experience is not what happens to a man, it is what a man does with what happens to him. ", author: "Aldous Huxley"},
    {q: "Imagination rules the world. ", author: "Napoleon Bonaparte"},
    {q: "Adversity has the effect of eliciting talents which, in prosperous circumstances, would have lain dormant. ", author: "Horace"},
    {q: "It isn't that they can't see the solution, it's that they can't see the problem. ", author: "G.K. Chesterton"},
    {q: "Facts are stubborn, but statistics are more pliable. ", author: "Mark Twain"},
    {q: "All truth goes through three steps:  First, it is ridiculed. Second, it is violently opposed.  Finally, it is accepted as self-evident. ", author: "Arthur Schopenhauer"},
    {q: "An invasion of armies can be resisted; an invasion of ideas cannot be resisted. ", author: "Victor Hugo"},
    {q: "Pain is inevitable but misery is optional. ", author: "Barbara Johnson"},
    {q: "Beware of defining as intelligent only those who share your opinions. ", author: "Ugo Ojetti"},
    {q: "If we knew what it was we were doing, it would not be called research, would it? ", author: "Albert Einstein"},
    {q: "To believe a thing is impossible is to make it so. ", author: "French proverb"},
    {q: "Simplicity is the ultimate sophistication. ", author: "Leonardo da Vinci"},
    {q: "To be simple is to be great. ", author: "Ralph Waldo Emerson"},
    {q: "The trouble about man is twofold.  He cannot learn truths which are too complicated; he forgets truths which are too simple. ", author: "Dame Rebecca West"},
    {q: "Everything should be as simple as it is, but not simpler. ", author: "Albert Einstein"},
    {q: "That you may retain your self-respect, it is better to displease the people by doing what you know is right, than to temporarily please them by doing what you know is wrong. ", author: "William J. H. Boetcker"},
    {q: "Many of life's failures are people who did not realize how close they were to success when they gave up. ", author: "Thomas Edison"},
    {q: "Hitch your wagon to a star. ", author: "Ralph Waldo Emerson"},
    {q: "If you knew how much work went into it, you wouldn't call it genius. ", author: "Michelangelo"},
    {q: "I know God will not give me anything I can't handle. I just wish that He didn't trust me so much. ", author: "Mother Teresa"},
    {q: "If we did the things we are capable of, we would astound ourselves. ", author: "Thomas Edison"}
];

var ul = document.createElement('ul');

for (var i = 0; i < quotes.length; i++) {
    var quote = quotes[i];
    var li = document.createElement('li');
    li.innerHTML = quote.q + ' <strong>~' + quote.author + '</strong>';
    ul.appendChild(li);
}

document.body.appendChild(ul);
