var quoteIdx = 0;
var form = document.querySelector('form');
var ul = document.querySelector('#quotes');

function filterQuotes(filter) {
    results = [];

    for (var i = 0; i < quotes.length; i++) {
        // some quotes do not have an author, set author to undefined for these
        var author = quotes[i].author || 'undefined';

        // convert filter text and author value to upper case, so that we can do a case
        // insensitive comparison
        author = author.toUpperCase();
        filter = filter.toUpperCase();

        // check if the string filter is found within the author string
        if (author.indexOf(filter) !== -1) {
            results.push(quotes[i]);
        }
    }

    return results;
}

function updateUI(filter) {
    ul.innerHTML = '';
    results = filterQuotes(filter);
    for (var i = 0; i < results.length; i++) {
        renderQuote(ul, results[i]);
    }
}

function renderQuote(container, quote) {
    var li = document.createElement('li');
    li.innerHTML = `${quote.q} <strong>~${quote.author}</strong>`;
    container.appendChild(li);
}

form.addEventListener('submit', function (evt) {
    // prevent default form action which would cause page to reload
    evt.preventDefault();

    filterValue = evt.target['filter-text'].value;
    updateUI(filterValue);
});

// Show all quotes by default
updateUI('');
