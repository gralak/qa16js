var quoteIdx = 0;
var prevBtn = document.querySelector('#prev');
var nextBtn = document.querySelector('#next');
var quoteEl = document.querySelector('#quote');

function nextQuoteIdx() {
    if (++quoteIdx >= quotes.length) {
        quoteIdx = 0;
    }

    return quoteIdx;
}

function prevQuoteIdx() {
    if (--quoteIdx < 0) {
        quoteIdx = quotes.length - 1;
    }

    return quoteIdx;
}

function showNext() {
    showQuote(nextQuoteIdx());
}

function showPrev() {
    showQuote(prevQuoteIdx());
}

function showQuote(idx) {
    quoteEl.innerHTML = `${quotes[idx].q} <strong>~${quotes[idx].author}</strong>`;
}

prevBtn.addEventListener('click', showPrev);
nextBtn.addEventListener('click', showNext);

showQuote(0);

