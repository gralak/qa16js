// ------------------------------------------------------
// CHALLENGE C-2
// ------------------------------------------------------

// Use the Object.create() API to create three people objects ("mike", "joe", and "katie")
// Each person object should contain three properties: name, age, and location.
// Each person object should share the same prototype. This prototype object should define a function
// that prints a description of the person, by utilizing the name, age and location properties.

var personPrototype = {
    printDescription: function() {
        console.log(`${this.name} is ${this.age} years old and lives in ${this.location}!`);
    }
};

var mike = Object.create(personPrototype, {
    name: { value: 'Mike' },
    age: { value: 42 },
    location: { value: 'San Francisco' }
});

var joe = Object.create(personPrototype, {
    name: { value: 'Joe' },
    age: { value: 68 },
    location: { value: 'Portland' }
});

var katie = Object.create(personPrototype, {
    name: { value: 'Katie' },
    age: { value: 23 },
    location: { value: 'San Jose' }
});


mike.printDescription();
joe.printDescription();
katie.printDescription();
