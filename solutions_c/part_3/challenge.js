// ------------------------------------------------------
// CHALLENGE C-3
// ------------------------------------------------------

// Below is the code for a simple web application that let's you track employees.
// Each employee has a list of skills that can be modified.
// Unfortunately, there is a bug in this code. Can you find (and fix) the bug?

var template = document.querySelector('#employee-template');
var employeeListEl = document.querySelector('#employee-list');
var addEmployeeButtonEl = document.querySelector('#add-employee');

addEmployeeButtonEl.addEventListener('click', addEmployee);

var baseEmployee = {
    skills: []
};

function addEmployee() {
    var name = prompt('What is the employee\'s name?');
    var newEmployee = Object.create(baseEmployee);
    newEmployee.name = name;
    newEmployee.skills = [];
    var domElement = createEmployeeEl(newEmployee);
    newEmployee.domElement = domElement;
}

function createEmployeeEl(employee) {
    var el = document.createElement('div');
    el.innerHTML = template.innerHTML;
    var li = el.firstElementChild;
    li.querySelector('.employee-name').innerHTML = employee.name;
    li.querySelector('button').addEventListener('click', addEmployeeSkill.bind(null, employee));
    employeeListEl.appendChild(li);
    return li;
}

function addEmployeeSkill(employee) {
    var skill = prompt('What is the skill?');
    employee.skills.push(skill);
    var skillsEl = employee.domElement.querySelector('.employee-skills');
    skillsEl.innerHTML = '';
    for (var i = 0; i < employee.skills.length; i++) {
        insertSkill(skillsEl, employee.skills[i]);
    }
}

function insertSkill(el, skill) {
    var li = document.createElement('li');
    li.innerHTML = skill;
    el.appendChild(li);
}
