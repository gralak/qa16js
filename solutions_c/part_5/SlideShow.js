// ------------------------------------------------------
// CHALLENGE C-5
// ------------------------------------------------------

// Create a new SlideShow control
function SlideShow(containerEl) {
    this.containerEl = containerEl;
    this.init();
}

// CSS class name to indicate a slide is currently selected (visible)
SlideShow.prototype.selectedCSSClass = 'state-selected';

// Initialize the SlideShow control
SlideShow.prototype.init = function() {
    // Add data attribute to container so that it is styled correctly
    this.containerEl.setAttribute('data-ui-control', 'SlideShow');

    // Hide all slides
    const slides = this.slides();
    slides.forEach(this.hide, this);

    // Show the first slide
    this.show(slides[0]);
};

// Show the next slide
SlideShow.prototype.next = function() {
    const slides = this.slides();
    const idx = this.currentIdx();
    this.hide(slides[idx]);
    var nextIdx = idx + 1;
    if (nextIdx >= slides.length) {
        nextIdx = 0;
    }
    this.show(slides[nextIdx]);
};

// Show the previous slide
SlideShow.prototype.prev = function() {
    const slides = this.slides();
    const idx = this.currentIdx();
    this.hide(slides[idx]);
    var nextIdx = idx - 1;
    if (nextIdx < 0) {
        nextIdx = slides.length - 1;
    }
    this.show(slides[nextIdx]);
};

// Get all slide elements as an array
SlideShow.prototype.slides = function() {
    return Array.prototype.slice.call(this.containerEl.children, 0);
};

// Get currently displayed slide index
SlideShow.prototype.currentIdx = function() {
    let idx = -1;
    const slides = this.slides();
    for (var i = 0; i < slides.length; i++) {
        if (slides[i].classList.contains(this.selectedCSSClass)) {
            idx = i;
            break;
        }
    }
    return idx;
};

// Show a specific slide
SlideShow.prototype.show = function(slide) {
    slide.classList.add(this.selectedCSSClass);
};

// Hide a specific slide
SlideShow.prototype.hide = function(slide) {
    slide.classList.remove(this.selectedCSSClass);
};
