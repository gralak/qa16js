// ------------------------------------------------------
// CHALLENGE C-1
// ------------------------------------------------------

// window.setInterval is an API that will call a function continually,
// with an optional delay between calls. The code below is intended to display
// a live clock, which updates once per second. However, there is a bug. Can you fix it?

var hourEl = document.querySelector('.hour');
var minEl = document.querySelector('.minute');
var secEl = document.querySelector('.second');

updateClock();
setInterval(updateClock, 1000);

function updateClock() {
    var hour, min, sec;
    var now = new Date();

    sec = now.getSeconds();
    min = now.getMinutes();
    hour = now.getHours();

    secEl.innerHTML = sec;
    minEl.innerHTML = min;
    hourEl.innerHTML = hour;
}
