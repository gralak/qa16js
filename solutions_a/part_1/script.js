// ------------------------------------------------------
// CHALLENGE A-1
// ------------------------------------------------------

// 1. Write a function that will add two numbers together.
// 2. Call your function and print the results.
//
// REMINDER: you can use alert('text'); or console.log('text');
//           to print to the screen.

function add(a, b) {
    return a + b;
}

var sum = add(10, 20);
alert(sum);
