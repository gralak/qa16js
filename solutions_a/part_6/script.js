// ------------------------------------------------------
// CHALLENGE A-6
// ------------------------------------------------------

// Let's write some simple password protection for our page!
// Use prompt() again to ask the user for their password.
// Don't let the user continue until they supply the secret password
// "abc123". Keep prompting the user until they supply the correct password.
//
// After the correct password is supplied, print the text "Correct!".
//
// BONUS: after 5 attempts, lock the user out so they cannot keep guessing.
//        (they can still try again by reloading the page).


var secret = 'abc123';
var attempt = '';

while (attempt !== secret) {
    attempt = prompt('What is your password?');
}

alert('Correct!');

// ------------------------------------------------------
// Bonus
// ------------------------------------------------------

var secret = 'abc123';
var attempt = '';
var successful = false;

for (var i = 0; i < 5; i++) {
    var attempt = prompt('What is your password?');
    if (attempt === secret) {
        alert('Correct!');
        successful = true;
        break;
    }
}

if (!successful) {
    alert('You are locked out');
}
