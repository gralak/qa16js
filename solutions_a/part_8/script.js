// ------------------------------------------------------
// CHALLENGE A-8
// ------------------------------------------------------

// Let's write a function that will format a person's name. Here
// are the rules we must follow:
//
//  - your function will be called with either 2 or 3 arguments, depending
//    on whether the person has a middle name or not
//  - the first argument will be the firstname
//  - the second argument will be either the middle or lastname
//  - the third argument will be the lastname (if there was a middle name)
//
//  - the formatted name should be "Lastname, Firstname, MIDDLENAME"
//  - The lastname is capitalized
//  - The firstname is capitalized
//  - The middlename, if there is one, should be all uppercase
//
// Example input and output:
//
//  fmtName('seth', 'thomas', 'lee'); // returns "Lee, Seth, THOMAS"
//  fmtName('seth', 'Lee'); // returns "Lee, Seth"
//  fmtName('SARAH', 'JONES'); // returns "Jones, Sarah"
//  fmtName('SARAH', 'lucy', 'JONES'); // returns "Jones, Sarah, LUCY"
//
// HINT: you can look up the documentation for strings on MDN
// <https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String>

function fmtName() {
    var first, last, middle;
    var name = [];

    if (arguments.length === 2) {
        first = arguments[0];
        last = arguments[1];
    } else {
        first = arguments[0];
        middle = arguments[1];
        last = arguments[2];

        name.push(middle.toUpperCase());
    }

    name.unshift(capitalize(first));
    name.unshift(capitalize(last));

    return name.join(', ');
}

function capitalize(str) {
    return str[0].toUpperCase() + str.slice(1).toLowerCase();
}

var testcases = [
    { expected: 'Lee, Seth, THOMAS', actual: fmtName('seth', 'thomas', 'lee') },
    { expected: 'Lee, Seth', actual: fmtName('seth', 'Lee') },
    { expected: 'Jones, Sarah', actual: fmtName('SARAH', 'JONES') },
    { expected: 'Jones, Sarah, LUCY', actual: fmtName('SARAH', 'lucy', 'JONES') }
];

for (var i = 0; i < testcases.length; i++) {
    if (testcases[i].actual !== testcases[i].expected) {
        console.error('Test', i, 'FAILED. Expected:',
                      testcases[i].expected, '; Actual:', testcases[i].actual);
    } else {
        console.log('Test', i, 'PASSED!');
    }
}
