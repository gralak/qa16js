// ------------------------------------------------------
// CHALLENGE A-7
// ------------------------------------------------------

// Write some code that will iterate over the `people` array. Print
// the name of each person who is older than 18.
//
// Can you do this using a for loop? how about a while loop?

var people = [
    { name: 'seth', age: 99 },
    { name: 'tom', age: 8 },
    { name: 'jane', age: 32 },
    { name: 'todd', age: 2 },
    { name: 'joe', age: 22 }
];

function printAdults() {
    for (var i = 0; i < people.length; i++) {
        var person = people[i];
        if (person.age > 18) {
            console.log(person.name);
        }
    }
}

printAdults();
