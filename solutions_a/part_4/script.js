// ------------------------------------------------------
// CHALLENGE A-4
// ------------------------------------------------------

// This function should print all even numbers between 0 - 100
// It looks like it is printing ALL numbers, including odd numbers.
// Can you fix this to work correctly?

function countEven() {
    for (var i = 0; i < 100; i++) {
        if (i % 2 === 0) {
            console.log(i, '...');
        }
    }
}

countEven();
