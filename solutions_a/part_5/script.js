// ------------------------------------------------------
// CHALLENGE A-5
// ------------------------------------------------------

// A simple way to get input from the user is to utilize the built-in
// prompt() function.
//
// Example:
//
//    var name = prompt('what is your name?');
//
// The `name` variable will now contain whatever text the user supplied.
//
// Using prompt(), ask the user for their favorite color. Then respond to the
// user with alert(), following these rules:
//
//   - if color is blue, then alert "me too!"
//   - if color is black, then alert "bad choice!"
//   - for any other color, alert "hmm that is not very interesting"

var answer = prompt('What is your favorite color?');

switch (answer) {
    case 'blue':
        alert('me too!');
        break;
    case 'black':
        alert('bad choice!');
        break;
    default:
        alert('hmm that is not very interesting');
        break;
}
