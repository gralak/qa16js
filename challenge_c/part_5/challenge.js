// ------------------------------------------------------
// CHALLENGE C-5
// ------------------------------------------------------

// The code below instantiates a "SlideShow" UI widget.
// The idea is that one image should be displayed at a time,
// and the user may click the next or previous button to
// cycle through the images.
//
// There are several bugs which prevent the code from working
// as intended. Find and fix all the bugs! :)
//
// Bonus: can you make the slide show loop when it reaches the end?

const containerEl = document.getElementById('my-slide-show');
const show = new SlideShow(containerEl);

document.querySelector('button#next').addEventListener('click', show.next);
document.querySelector('button#prev').addEventListener('click', show.prev);
