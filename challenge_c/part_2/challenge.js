// ------------------------------------------------------
// CHALLENGE C-2
// ------------------------------------------------------

// Use the Object.create() API to create three people objects ("mike", "joe", and "katie")
// Each person object should contain three properties: name, age, and location.
// Each person object should share the same prototype. This prototype object should define a function
// that prints a description of the person, by utilizing the name, age and location properties.

