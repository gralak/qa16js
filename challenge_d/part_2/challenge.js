// ------------------------------------------------------
// CHALLENGE D-2
// ------------------------------------------------------

// The code below implements a few utility functions. We
// want to write unit tests to validate that our code works
// the way we expect.
//
// In this challenge, you will implement a simple testing
// framework which will allow you to define unit tests,
// run the tests, and report the results.
//
// Note: the code we are testing (below) may contain bugs.

function countLetters(text) {
    // Some expected input => output
    // 'abc 123' => 4
    // 'Hello World' => 10
    // 'hi' => 2
    const letters = 'abcdefghijklmnopqrstuvwxyz';
    return letters.split('').reduce((count, l) => {
        if (letters.indexOf(l) !== -1) {
            count++;
        }
        return count;
    }, 0);
}

function addNumbers() {
    // Some expected input => output
    // 3, 4, 5 => 12
    // 1 => 1
    // 9, 10, 10, 100 => 129
    var result = 0;
    for (var i = 0; i < arguments.length; i++) {
        result += arguments[i];
    }
    return result;
}
