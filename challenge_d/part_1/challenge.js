// ------------------------------------------------------
// CHALLENGE D-1
// ------------------------------------------------------

// Implement the "LongestWord" function below. This function
// should return the longest word in a string that is passed
// to the function. If there is a tie, return the first word
// in the set of the longest words.
//
// Example, this should return "there":
//
//   longestWord("hi there, how are you?");
//
// Once you implement the function correctly, reload the page
// in your browser. The web page will turn green.

function longestWord(text) {
    // your code goes here.
    return text;
}
