var expected, actual, failures;

failures = 0;

// TEST #1
expected = 'there';
result = longestWord('hi there how are you?');

if (expected !== result) {
    alert(`Test #1 -- Expected: ${expected}; Actual: ${result}`);
    failures++;
}

// TEST #2
expected = 'hello';
result = longestWord('hello, what is the best type of food?');

if (expected !== result) {
    alert(`Test #1 -- Expected: ${expected}; Actual: ${result}`);
    failures++;
}

// TEST #3
expected = 'abracadabra';
result = longestWord('abracadabra mini foobar good sir');

if (expected !== result) {
    alert(`Test #1 -- Expected: ${expected}; Actual: ${result}`);
    failures++;
}

// Change page to reflect results
if (failures === 0) {
    document.body.classList.add('pass');
} else {
    document.body.classList.add('fail');
}
